from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
from coin_database import Base, Users, Coins, Balances, Transactions


engine = create_engine('sqlite:///userBalances.db')
# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the database
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()

# Precious stones
#
user = Users(name='Nicolas', country='France', email='nferrari3444@gmail.com',public_key='8855440039', 
             userName= 'nferrari1984', password='inteligentCity')
session.add(user)
session.commit()

user = Users(name='Jhon', country='United States', email='jhonKay@gmail.com',public_key='5577889933',
             userName= 'JhonFromUsa1985', password ='workingHard')
session.add(user)
session.commit()

user = Users(name='Paul', country='Mexico', email='paulFernandez22@gmail.com',public_key='3399225576',
            userName = 'PaulMcKinsey1980', password = 'cryptoForAll')
session.add(user)
session.commit()

user = Users(name='Susan', country='Canada', email='susan_the_king33@gmail.com',public_key='8845325571',
             userName = 'Susan1975', password = 'alwaysLearning19')
session.add(user)
session.commit()

balance= Balances(created_date=datetime.datetime.now().date(), lastUpdate = datetime.datetime.now().date(),
                  user_id=1, coin='BTC',amount= 155)
session.add(balance)
session.commit()

balance= Balances(created_date=datetime.datetime.now().date(), lastUpdate = datetime.datetime.now().date(),
                  user_id=2, coin='ETC',amount=120)
session.add(balance)
session.commit()

balance= Balances(created_date=datetime.datetime.now().date(), lastUpdate = datetime.datetime.now().date(),
                  user_id=3, coin='LTC',amount= 119)
session.add(balance)
session.commit()

balance= Balances(created_date=datetime.datetime.now().date(), lastUpdate = datetime.datetime.now().date(),
                  user_id=4, coin='XLM',amount= 200)
session.add(balance)
session.commit()
