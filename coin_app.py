
from flask import Flask, render_template, request, redirect, url_for, flash, jsonify
from flask import session as user_session
from coin_database import Base, Users, Coins, Balances, Transactions
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime


engine = create_engine('sqlite:///userBalances.db?'
                       'check_same_thread=False')


Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

app = Flask(__name__)

@app.route('/')
def home():
    '''
    Function that allow users to login into the application. 

    Args:
        This method does not have arguments
    Returns:
        if the user is not logged in the aplication, it display the login screen with the required fields such 
        as username and password. If the user is logged into the application, it returns the main panel of 
        functionalities on the network. 
    '''

    if not user_session.get('logged_in'):
        return render_template('login.html')
    
    else:
        user_id = user_session['user_id']
        queryUser = session.query(Users).filter_by(id=user_id).one()
        name = queryUser.name
        return render_template('useNetwork.html',user_id=user_id, name=name) 

@app.route('/login', methods=['POST'])
def do_admin_login():
    """
    Web Form to logged into the application
    Args:
        This method does not have arguments
    Returns:
        return main panel of the network if the user is correctly logged into the application. If the user
        credentials(userName and password) are wrong, the function redirect to the login form
    """

    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])

    query = session.query(Users).filter(Users.userName.in_([POST_USERNAME]), Users.password.in_([POST_PASSWORD]) )
    user = query.first()
    if user:
        user_session['logged_in'] = True
        # Store the user_id in the dictionary of the session that was renamed as user_session. This is  
        # important because to return to the home page, the useNetwork template, require the user_id
        user_session['user_id'] = user.id
        
        return UseNetwork(user.id)
    else:
        flash('InValid user name or password!')
    
        return home()

@app.route("/logout")
def logout():
    """
    Finish the session of the user on the network
    Args:
        This method does not have arguments
    Returns:
        set the looged_in key from the user_session context object to False, and return to the home site. As the 
        logged_in is false, the user need to login next time he/she want to access to the application
    """
    
    user_session['logged_in'] = False
    return home()

@app.route('/network/<int:user_id>')
def UseNetwork(user_id):
    '''
    Show different operations that the user can do on the application, such as Check Balance, Perform a transaction,
    Create a new coin, visualize all coins and check all transactions over the network.
    Args:
        user_id: int, identification number of the user in the application. Is the primary key of the Users table        
    
    Returns:
        template-page with all functionalites that the user can perform on the network
    '''
    
    queryUser = session.query(Users).filter_by(id=user_id).one()
    name = queryUser.name
    return render_template('useNetwork.html', user_id = user_id,name=name) 

    

@app.route('/coins/new/', methods=['GET','POST'])
def newCoin():
    """
    Create a new Coin on the app
    Args:
        This method does not have arguments
    Returns:
        returns a new coin that would be stored
        on the database of the app
    """
    if request.method == 'POST':

        coin = request.form['CoinName']
        queryCoin = session.query(Coins).filter_by(name=coin)
        checkCoin = queryCoin.first()
        if checkCoin:
            flash("Cannot create a coin which is already created")
            return render_template('CreateCoins.html')
        
        # Create the new coin
        new_coin = Coins(created_date = datetime.datetime.now().date(),name=request.form['CoinName'],
                              value=request.form['CoinValue'],unity=request.form['Unit'])
        
        
        session.add(new_coin)
        session.commit()
        flash('New Coin Created Successfully')
        return redirect(url_for('coins'))
    else:
        return render_template('CreateCoins.html')

@app.route('/viewCoins/')
def coins():
    '''
    Provide all coins created by the users within the application
    
    Args:
        This method does not have arguments
    Returns:
        return all the coins that can be used in the network with information such as date creation, value 
        and unit
    '''

    coinsData = session.query(Coins).all()

    return render_template('showCoins.html', coins= coinsData)

@app.route('/transactions/')
def showTransactions():
    '''
    Show all the transactions among all users on the Network
    Args:
        This method does not have arguments
    Returns:
        return all the transactions between users over the network. Each transaction has information about
        the sender, recipient, coin, amount and a brief description of the transaction 
        
    '''

    transactions = session.query(Transactions).all()

    return render_template('showTransactions.html', transactions = transactions)

@app.route('/balances/<int:user_id>')
def showBalance(user_id):
    '''
    Display the balances on different coins of the user on the Application
    Args:
        user_id: int, identification number of the user in the application. Is the primary key of the Users table
    Returns:
        return balance information for each user on different coins. For each coin, the service display the 
        date of the account creation, last update of the account, coin and amount holdings
    '''

    balance = session.query(Balances).filter_by(user_id=user_id).all()
    queryUser = session.query(Users).filter_by(id=user_id).one()
    name = queryUser.name
    return render_template('Balances.html', balance=balance, name=name)

@app.route('/Payment/<int:user_id>', methods=['GET','POST'])
def sendTransaction(user_id):
    '''
    Send and receive funds between users on the network
    Args:
        user_id: int, identification number of the user in the application. Is the primary key of the Users table
    Returns:
        return new transaction in the Transactions table and updated balances for the sender and recipient in the 
        coin of the transaction on the Balances table. If the recipient does not hold the coin in it account,
        a new transaction is created on the recipient Balance table. Contrary, if the recipient hold the coin in
        it account, the balance is updated.
    '''
         
    if request.method == 'POST':
        sender = str(request.form['sender']) 
        # Upper the first letter of sender to have same format than database records
        sender = sender.title() 

        recipient = str(request.form['recipient']) 
        # Upper the first letter of sender to have same format than database records
        recipient = recipient.title() 
        
        coin = str(request.form['coin'])
        coin = coin.upper()
        description = str(request.form['description'])
        
        # Check if the sender name is correct, based on database records
        senderEntry = session.query(Users).filter_by(name=sender)
        senderResult = senderEntry.first()
        if not senderResult:
            flash('Invalid Sender Data')
            return  render_template("transaction.html",user_id = user_id) 

        # Test if the sender name correspond to the sender account. This check is to guarantee that the sender
        # that was typed is the owner of the account.

        if senderResult:
            senderId = senderResult.id 
            if user_id != senderId:
                flash('This Account does not correspond to that sender')
                return  render_template("transaction.html",user_id = user_id) 
            

        # Check if the recipient name is correct, based on database records
        recipientEntry = session.query(Users).filter_by(name=recipient)
        recipientResult = recipientEntry.first()
        if not recipientResult:
            flash('Invalid Recipient Data')
            return render_template("transaction.html", user_id = user_id)

        
        # Check if the coin of the transaction belong to a valid coin on the application
        coinEntry = session.query(Coins).filter_by(name=coin)
        coinResult = coinEntry.first()
        if not coinResult:
            
            flash("Coin not in the application, create the new coin")
            return redirect(url_for('newCoin'))

        
        # The two tables: Balances and Users are related tables with the field user_id in the Balances being the 
        # foreign key of the field id(primary key) in Users table. So, first we will get the user id with the
        # names of the sender and recipient.

        sender = session.query(Users).filter_by(name=sender).one()
        senderId = sender.id
        recipient = session.query(Users).filter_by(name=recipient).one()
        recipientId = recipient.id 

        senderBalance = session.query(Balances).filter_by(user_id=senderId,coin=coin)
        senderBalance = senderBalance.first()
        
        # Check if the sender hold funds on the coin that want to send money
        if not senderBalance:
            flash("Cannot process Transaction because Sender doesn't have that coin")
            return render_template("transaction.html", user_id = user_id)
    
        # Check if sender has enough funds in it account to process transaction
        if senderBalance.amount - float(request.form['amount']) <0:
            flash("User doesn't have enough funds to process this transaction")
            return render_template("transaction.html", user_id = user_id)
        

        # Confirm and commit transaction if the operation pass all the filters above
        transfer = Transactions(time_= datetime.datetime.now().replace(microsecond=0),
                                sender=request.form['sender'], recipient = request.form['recipient'],
                                amount = request.form['amount'], coin = coin, description=description)
    
        session.add(transfer)
        session.commit()
       

        # After trasanction is commited, it is necessary to update balances of sender and recipient users
        # on the correspondent coin. The steps to achieve this are the following:
        # 1- Retrieve current balance of each user on the balances table
        # 2- If recipient doesnt hold the coin, insert a record in the Balances table with the recipient 
        # information. If recipient hold the coin in it account, proceed to update current balance on that coin
        # 3- Update each balance in the database by subtracting the amount to the sender and adding the
        # amount to the recipient

      
        # Check if the recipient has the coin in it account. If already has the coin, we need to update the 
        # amount holding, else we create a new record adding a new row in the Balances table for the recipient.
        recipientBalance = session.query(Balances).filter_by(user_id=recipientId,coin=coin)
        recipientBalance = recipientBalance.first()
        if not recipientBalance:
            newEntry = Balances(user_id = recipientId,lastUpdate = datetime.datetime.now().replace(microsecond=0),
            coin=coin, amount=float(request.form['amount']))
            session.add(newEntry)
            session.commit()

        else:
            recipientBalance.amount = recipientBalance.amount + float(request.form['amount'])
            recipientBalance.lastUpdate = datetime.datetime.now().replace(microsecond=0)
            session.add(recipientBalance)
            session.commit()

        # Update sender balance and lastUpdate field after transaction
        senderBalance.amount = senderBalance.amount - float(request.form['amount'])
        senderBalance.lastUpdate = datetime.datetime.now().replace(microsecond=0)
        
        
        session.add(senderBalance)
        session.commit()

    
        flash('New Transaction Created Successfully')
        return redirect(url_for('UseNetwork',user_id=user_id))
    
    else:
       
        return render_template('transaction.html', user_id=user_id)

        

if __name__ == '__main__':
    app.secret_key = 'supersecretkey'
    app.debug = True
    app.run(host='0.0.0.0', port=8000)