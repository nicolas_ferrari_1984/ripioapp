import os 
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, Float, DateTime
import datetime

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()


class Users(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    country = Column(String(250), nullable=False)
    email = Column(String(50), nullable=False)
    public_key = Column(String(100),nullable=False)
    userName = Column(String(50), nullable = False)
    password = Column(String(100), nullable=False)



class Coins(Base):
    __tablename__ = 'coins'
    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.datetime.now().date())
    name = Column(String(100), nullable=False)
    value = Column(Float, nullable=False)
    unity = Column(String(50),nullable=False) 


class Balances(Base):
    __tablename__ = 'balance'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    created_date = Column(DateTime, default=datetime.datetime.now().date())
    lastUpdate = Column(DateTime, default=datetime.datetime.now().date())
    coin = Column(String(100),nullable=False)
    amount = Column(Float, nullable=False)
    users = relationship(Users)

class Transactions(Base):
    __tablename__ = 'Transaction'
    time_ = Column(DateTime, primary_key=True)
    coin = Column(String(100),nullable=False)
    amount = Column(Float, nullable=False)
    sender = Column(String(250), nullable=False)
    recipient = Column(String(250), nullable=False)
    description = Column(String(500))


engine = create_engine('sqlite:///userBalances.db')


Base.metadata.create_all(engine)
