# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This project is an application that propose the use of a coin or multiple coins between users that can send
and recieve these coins in their accounts for payments and transfer of money. In the main page of the application 
can be found the different functionalites such as:

Check User Balance on different Coins
Create New Coin
Send Payment
Visualize all Transaction over the Network
Visualize all coins on the Network


### Technical Features ###

The application was built with 
the Flask Framework of Python and use SQLAlchemy as a Object Relational Mapper(ORM) to use the data model
over the application. 

For the frontend side, it uses the Jinja2 framework which is provided by flask to render templates and add 
styles for these templates. In conclusion, Flask was used for the backend, sqlite3 and SQLAlchemy for the 
data model and Jinja2 as well as html and css for template creation, designing and styling. 

The structure of the project is composed by the templates folder which store all templates used on the 
application, the static folder that has a file that style some parts of the application, a database model
filecalled coin_database.py, and the coin_app.py to run the application. 

The file populate_tables.py was used to insert the first values on the tables Users and Balances. To get 
access to the application, each user was created in the User table should login to the app, with the user 
name and passord. 

The aplication contains unit test in specific places to control multiple things. These controls are performed
in the moment of a new transaction where are tested, the sender, the recipient, the sender amount as well
as the the coin of the transaction. As well there are others control on the Coin creation functionality and 
in the login function. 

Lastly, the file userBalances.db is an sqlite3 file that has all database records for the application.

Tell me if there are questions or comments. I will guide.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact